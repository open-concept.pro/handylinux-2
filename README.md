HANDYLINUX
==========

**HandyLinux** c'est l'accessibilité pour tous et la liberté pour chacun d'évoluer à son gré.

Basée sur **Debian GNU/Linux** avec **XFCE**, un environnement de bureau rapide, léger et stable, HandyLinux est **sûre, pratique et gratuite**.

Conçue pour faciliter l'accès à l'informatique à ceux qui débutent, les enfants, les seniors et ceux qui sont en quête de simplicité. Pour cela elle comprend :

- un **pack simplifié d'applications** pour les tâches courantes (navigation sur le net, mail, suite office, logiciels photo/audio/vidéo) facilement accessibles depuis le **HandyMenu**.
- la **logithèque Debian** riche de milliers d'applications pour les aventuriers.
- une **aide complète** en ligne et dans la distribution ainsi qu'une initiation intégrée afin de répondre à vos questions et accompagner votre progression.
- Si vous êtes en situation de handicap léger, la rubrique "access" est là pour faciliter l'accès aux différentes fonctions de l'ordinateur : lecteur d'écran ORCA, loupe d'écran Loupy, sélectionneur rapide d'interface et les modules du navigateur : clavier virtuel et synthèse vocale.

----------

- [site principal](https://handylinux.org)
- [forum d'entraide](https://handylinux.org/forum)
- [documentation fr/en](https://handylinux.org/wiki)
- [blog du projet](http://blog.handylinux.org)
- [salon IRC-Jabber](http://irc.handylinux.org)
- [tutoriels vidéos](https://handylinux.org/tutos)

----------

- [web startpage](https://handylinux.org/start)
- [logos officiels](https://handylinux.org/artwork)
- [galerie handylinux](https://handylinux.org/wallpapers)
- [vidéos officielles](https://handylinux.org/videos)
- [commandes GNU/Linux](http://cli.handylinux.org)

----------

- [dépôts officiels](http://repo.handylinux.org)
- [sources et archives](https://handylinux.org/sources)
- [mail/contact](contact@handylinux.org)

----------

applications incluses
---------------------
- environnement de bureau : XFCE
- gestionnaire de fichiers : Thunar
- navigateur internet : Iceweasel (firefox rebranded)
- client de messagerie : Icedove (thunderbird rebranded)
- clients torrent P2P : Transmission & BTshare
- lecteur vidéo : VLC
- lecteur audio : Clementine
- web radio : RadioTray
- visionneuse d'images : Ristretto & Cyclope
- éditeur d'images : the Gimp (interface simplifiée)
- suite bureautique : LibreOffice
- éditeur de texte : Mousepad
- gestionnaire d'archives : Xarchiver
- recherche : Catfish
- communication : Skype-installer
- aide à distance : TeamViewer-installer
- suite d'outils pour l'accessibilité : lecteur d'écran ORCA, loupe d'écran loupy, clavier virtuel, menu simplifié
- gestionnaire d'impression et de numérisation
- aide française/anglaise complète en ligne et dans la distro + initiation intégrée

matériel requis
---------------
**HandyLinux** s'installe sur tout ordinateur moderne (PIV ou supérieur) pourvu de 512M de mémoire, nécessite 3,7 GB d'espace disque minimum.

----------

changelog 2.3
-------------
- retour de pciutils (merci dyp & coyotus)
- prise en charge de la mise à jour par handy-upgrade-manager (merci thuban)
- ajout de aptitude pour être conforme à la documentation Debian (merci dyp)
- ajout de handy-upgrade-manager pour les mises à jour notifiées par handy-update-notifier (merci thuban)

changelog 2.2
-------------
- mise à jour de la base : Debian 8.2
- mise à jour de Iceweasel - v40
- suppression des modules de lisibilité en faveur du mode de lecture intégré à Iceweasel
- remplacement de µblock par µblock.origin
- ajout de qwant, wiktionnaire et openstreetmap comme moteurs alternatifs (merci ideefixe et èfpé)
- ajout de info4forum qui remplace hardinfo (merci thuban & coyotus)
- passage du script kernel-cleaner en background (merci chrisinfo)
- passage du HandyMenu en v3 avec configuration graphique complète (merci thuban)
- ajout de cyclope comme visionneuse minimale alternative (merci thuban)
- mise à jour du "À propos de mon HandyLinux" (merci coyotus)
- ajout de ntfs-config (merci dyp)
- ajout de fonts-opendyslexic depuis les dépôts (merci thuban)
- intégration de gimp'light config
- remplacement de la loupe d'écran vmg par loupy (merci thuban)
- passage du forum et du wiki en https (merci aux donateurs)
- correction des liens et marque-pages pour le https
- suppression de pulseaudio (installé uniquement avec skype ou teamviewer)
- correction de l'icône du mixer (merci Jambalak)
- ajout d'actions personnalisées à thunar (merci thuban)
- mise en paquet des scripts pour thunar afin d'assurer la mise à jour et l'ajout de scripts
- ajout de curl, sox et mpg321 pour les actions personnalisées
- ajout de unbound comme resolveur DNS local pour éviter le filtrage des FAI (merci thuban)
- passage du sources.list en httpredir.debian.org pour accélérer la gestion des paquets et soulager les serveurs (merci thuban)
- suppression de l'éditeur de menu XFCE (pas totalement fonctionnel) inutile depuis HandyMenu-v3
- suppression des dépôts backports (on ne garde que les dépôts mozilla-backports)
- passage de la documentation intégrée en paquets pour faciliter la mise à jour.
- ajout d'une version HandyLinux-amd64 en test

changelog 2.1
-------------
- mise à jour de la base : Debian 8-1
- mise à jour de Iceweasel : passage à la version backports + définition des applications par défaut pour les .torrent et liens "magnet"
- suppression des fichiers de configuration apt pointant sur les dépôts "experimental" (résidu de la phase de test beta)
- suppression d'aptitude (utilisé pour l'upgrade mais plus maintenant, merci thuban)
- passage de l'update-notifier dans les dépôts handylinux (merci thban et coyotus)
- correction du bug pixup.sh/py pour l'upload sur toile-libre (merci thuban)
- mise à jour de la documentation
- ajout de jeux : flobopuyo et Ltris 
- définition des applications par défaut pour l'ouverture des fichiers

changelog 2.0
-------------
- mise à jour de la base : passage à Debian-8 "Jessie" (merci Debian)
- mise à jour du handymenu et des outils *handylinux*
- refonte admin du site 'handylinux.org' et mise en place des sous-domaines
- ajout de BTshare, un outil de partage P2P simplifié (merci thuban)
- ajout des actions personnalisées pour Thunar (merci coyotus & thuban)
- passage de Skype en option (merci coyotus)
- intégration du lecteur d'écran gnome-orca (merci totophe56)
- ajout des lanceurs outils/sociaux/galerie...
- ajout du témoin de mot de passe (merci coyotus)
- définition de "handylinux" comme hostname par défaut (merci fredbezies)
- ajout des Modèles dans le dossier utilisateur (merci dyp)
- ajout de menulibre pour l'édition des menus XFCE
- ajout de bash-completion (merci torxxl)
- ajout de cups-pdf (merci rb)
- ajout de transmission pour la gestion du partage P2P
- ajout de thèmes pour Iceweasel
- ajout des fonts carlito & caladea (eq. Calibri & Cambria) (merci thuban)
- ajout des modèles et config pour LibreOffice (merci thuban)
- ajout de unoconv pour l'exportation rapide en PDF (merci thuban)
- ajout de anacron et de la configuration apt (merci base10)
- ajout de command-not-found + fix bashrc (merci base10)
- ajout de la notification de mise à jour (merci coyotus)
- kernel
  - passage de 486 à 586 pour les vieux PCs
  - fusion des version 586 et 686-pae
  - ajout du script de détection/suppression du kernel inutile
- theming/graphics
  - refonte graphique du site/forum/wiki/blog
  - ajout du handylinuxlook
  - ajout du HandyTheme pour le changement rapide d'interface (merci Starsheep & thuban)
  - nouveau thème Slim "flat" par Starsheep
  - nouveaux walls par Starsheep
  - ajout de qt4-qtconfig pour vlc
  - ajout thèmes d'icônes additionnels (tango, gnome-colors, gilouche)
  - modification de l'îcone whiskermenu (merci julien)
- switch
  - remplacement du software-center par gnome-packagekit
  - remplacement de file-roller par xarchiver
  - remplacement de gnome-searchtool par catfish et mlocate (merci Elm et thuban)
  - remplacement de quodlibet par clementine
  - remplacement de shotwell par ristretto
  - remplacement de leafpad par mousepad comme éditeur de texte par défaut (merci coyotus)
  - remplacement de xpad par xfce4-notes-plugin pour une meilleure intégration au panel
- nettoyage
  - suppression de l'option de placement du HandyMenu en conflit avec XFCE (merci Caribou22)
  - suppression des préférences apt obsolètes pour LibreOffice
  - suppression des paquets obsolètes ntfsprogs/netatalk/fuse-utils/dfc/xfprint4/gtk3-engines-unico
  - suppression de xl-wallpaper : xfce gère maintenant les changement de fonds
  - suppression des dépôts compiz-handylinux
  - suppression de slingscold-launcher
  - suppression de bleachbit
  - suppression de zram
  - suppression de Minitube
  - suppression de phonon
  - suppression d'oggconvert
  - suppression du win32-loader
  - nettoyage des addons Iceweasel
  - nettoyage des walls
  - suppression des thèmes d'icônes Nitrux/NitruxButtons


changelog 1.7.1 -> 1.8
----------------------
- suppression du dépôt videolan, prise en charge de libdvdcss2 (merci coyotus)
- mise à jour d'Iceweasel en version 35
- désactivation de quelques plugins iceweasel pour améliorer le lancement (merci Xanatos)
- changement du moteur de recherche par défaut (StartPage) et ajout de moteurs (wikipedia-fr, searx, dailymotion...)
- mis à jour du handymenu : ajout des liens wikipédia et wallpapers
- ajout de gpart pour la récupération simple de données (merci dyp)
- suppression des musiques non-libres (merci On)
- ajout du lien vers Jamendo
- suppression des méta-paquets par défaut, ne reste qu'handylinux-desktop
- suppression de Skype des sources, mais toujours présent dans l'ISO.
- suppression de Teamviewer des sources
- ajout d'un installer pour Teamviewer en version 10 (merci coyotus)
- ajout de thèmes pour curseur (merci bruno, dyp et Ruz)
- ajout de thèmes gtk/xfwm Tron (merci Millie) et Pluto-gtk
- mise à jour redshift-config (merci Starsheep)
- ajout de pulseaudio pour skype et/ou teamviewer (merci coyotus)
- mise en place d'une nouvelle page d'accueil (merci Tiberias)

changelog 1.7 -> 1.7.1
----------------------
- reconstruction pour activer flash...

changelog 1.6.1 -> 1.7
----------------------
- passage à iceweasel-release comme navigateur par défaut
- ajout du firmware-ralink
- ajout de gpart pour la fonction de récupération de gparted
- ajout de yelp pour l’aide des logiciels
- nettoyage de la doc en fonction de la langue
- suppression du lanceur facebook au profit de framasoft
- ajout des lanceurs sociaux
- ajout de lien Diaspora
- ajout du thème tron par Millie
- mises à jour Debian

changelog compiz-1.6.1
----------------------
- ajout d'une session compiz en option
- mises à jour Debian
- intégration de la session compiz dans le processus de build
- ajout de la configuration gtk2/3

changelog 1.6 -> 1.6.1
----------------------
- mise à jour de skype en version 4.3
- suppression de l'appartenance à sudo pour les users supplémentaires
- passage de aspell/myspell à hunspell
- ajout de exfat-utils et exfat-fuse
- ajout du plugin de sélecteur de clavier xfce4-xkb-plugin
- refonte du processus de build pour régler le bug de l'autoremove et des méta-paquets
- mises à jour Debian

changelog 1.5 -> 1.6
--------------------
- amélioration du menu d'accessibilité (merci Irina)
- ajout de BigBuckBunny en exemple vidéo
- fusion des deux versions fr/en
- ajout du lancement live en mode sans échec
- ajout d'un sélecteur de clavier en session live
- intégration des méta-paquets handylinux-xxx
- ajout du fichier apt/preferences pour libreoffice en backports
- test version amd64 puis abandon car aucun besoin pour notre distribution
- retour du sélecteur de clavier à l'installation pour la version francophone
- passage à icedtea-7-plugin (merci coyotus)
- ajout de pepperflashplugin-nonfree et ttf-xfree86-nonfree pour chromium (merci dyp)
- passage au handymenu-2 avec outil de configuration intégré (merci manon)
- ajout des dépôts HandyLinux 'coming' commentés par défaut
- passage du handytri en paquet Debian
- ajout de hpijs-ppds pour les fichiers ppd (merci dYp)
- ajout de libmtp9 et libmtp-runtime (exit les warning modprobe, merci dYp)
- ajout de jmtpfs, mtp-tools et python-pymtp pour le transfert MTP (merci dyp)
- ajout magicfilter, djtools, librecode0, recode et lpr pour l'envoi à l'impression directe (merci dYp)
- refonte du processus de build
- mise à jour de teamviewer
- mise à jour de skype
- mise à jour Debian
- ajout de mscorefonts-installer
- ajout des outils iotop/iftop/dfc/colortail/most (merci lexdu)

changelog 1.4.2 -> 1.5
----------------------
- mise à jour de sécurité Debian
- mise à jour du noyau Linux
- mise à jour Chromium
- mise à jour du kernel
- ajout de l'invite de mise à jour au premier redémarrage
- mise à jour du handy-menu
- intégration du whiskermenu comme menu alternatif
- mise à jour de redshift-config
- ajout de fonts-opendyslexic
- ajout de cheese pour la webcam

changelog 1.4.1 -> 1.4.2
------------------------
- ajout de dvd+rw-tools, asunder et oggconvert
- entrée en piste des dépôts handylinux
- passage du filtre d'écran à redshift-config (config auto) par manon
- passage des lanceurs en exo-open dans le handy-menu et les lanceurs
- retour sur gitorious pour la gestion du code
- passage à system-config-printer pour la gestion de l'impression
- passage de handy-menu, slingscold et mpartage en installation externe
- ajout de gnome-font-viewer pour la gestion des polices de caractères
- ajout de l'outil de configuration pour le handymenu par manon
- suppression des lignes inutiles dans le touchpad-tap.sh
- ajout de hddtemps et lm-sensors
- rétablissement de la dernière étape de l'installeur Debian (retirer la clé usb)
- ajout du fichier network.mount pour éviter le montage automatique du réseau dans thunar

changelog 1.4 -> 1.4.1
----------------------
- bug du magnifier avec nvidia => rétablissement de la loupe d'écran classique
- rétablissement du panel de la 1.3

changelog 1.3 -> 1.4
--------------------
- conservation de GParted après installation
- passage de xsltproc dans les paquets
- ajout de blueman
- ajout de libasound2-plugins pour skype
- ajout de [xl-wallpaper](http://xl-wallpaper.net) de **Christophe Cagé**
- intégration des modifications redshit par **firepowi**
- ajout de gnome-system-tools pour la gestion des utilisateurs
- remplacement du lanceur "twitter" par "minitube"
- remplacement de xsane par simple-scan
- ajout de python-lxml pour la gestion du handymenu
- handy-menu : passage aux commandes "exo-open" + ajout de la persistance par **manon**
- coupure du réseau pendant l'install pour éviter les plantage en cas d'incident réseau
- ajout de mpartage par **manon**
- confguration du NumLockx intégré dans slimconf par **manon**
- inclure tap-to-clic.sh désactivé
- rétablissemement du réglage du hostname dans l'installeur
- passage à libreoffice-4(backports)
- ajout du script de triage automatique
- ajout du plugin xfce météo

changelog 1.2 -> 1.3
--------------------
- une seule instance pour le HandyMenu
- virer speed-dial à l'ouverture de chromium
- intégrer redshift avec un sélecteur de villes
- intégrer la documentation en format pdf dans la distribution pour une consultation hors-ligne
- remplacer squeeze par file-roller
- simplifier le handymenu
- remplacer gpicview par shotwell
- ajouter gparted dans le livecd
- ajouter minitube
- ajout de la gestion du bluetooth
- passage à teamviewer9
- changer les entrées handymenu avec les entrées génériques
- intégration du greffon de réglage du volume dans le panel
- une seule instance de la loupe d'écran
- ajouter un lanceur "negative" aux outils : xcalib

avant, c'était vraiment nawak :P

pour l'historique complet d'HandyLinux, visitez [la section dédiée](https://handylinux.org/wiki/doku.php/fr/handy_story) de notre documentation.

