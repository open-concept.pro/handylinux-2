# handylinux makefile arnault perret <arpinux@member.fsf.org>

all: 
	@echo "Usage: "
	@echo "make iso       : build handylinux2"
	@echo "make clean     : clean up build directories"
	@echo "make cleanfull : clean up build & cache directories"

iso: clean
	@echo "--------------------"
	@echo "building HandyLinux2"
	@echo "--------------------"
	lb build

clean:
	@echo "--------------------------"
	@echo "cleaning build directories"
	@echo "--------------------------"
	lb clean

cleanfull: clean
	@echo "----------------------------------"
	@echo "cleaning build & cache directories"
	@echo "----------------------------------"
	rm -R -f cache
	rm -f ./*.log

