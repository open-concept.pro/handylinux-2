#!/usr/bin/python
# -*-coding:utf-8-*
#######################
# handy linux welcome
# by arpinux
########################################################################

import pygtk
pygtk.require("2.0")
import gtk
import os

########################################################################
# main window
########################################################################
class HandyWelcome():
    def __init__(self):
	    interface = gtk.Builder()
	    interface.add_from_file("welcome.glade")
	    interface.connect_signals(self)

    def on_button1_clicked(self, widget):
        os.system("x-www-browser file:///usr/share/handylinux/guide.html &")

    def on_bienvenue_delete_event(self, widget, event):
	    gtk.main_quit()

########################################################################
if __name__ == "__main__":
  HandyWelcome()
  gtk.main()
