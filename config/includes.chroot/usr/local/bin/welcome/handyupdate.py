#!/usr/bin/python
# -*-coding:utf-8-*

import pygtk
pygtk.require("2.0")
import gtk
import os

class HandyUpdate():
	
	def on_button1_clicked(self, widget):
		gtk.main_quit()
		
	def on_button2_clicked(self, widget):
		os.system("gksudo apt-get update && gpk-update-viewer &")
		gtk.main_quit()
		
	def __init__(self):
		
		window = gtk.Window()
		window.set_title("HandyUpdate")
		window.connect("destroy", self.on_button1_clicked)
		window.set_default_size(300, 200)
		
		vBox = gtk.VBox()
		label = gtk.Label()
		label.set_text("\n    Vous venez d'installer HandyLinux ... merci :)    \n" +
						"\n    Votre système est prêt à être utilisé, cependant    \n" +
						"    selon la date de construction de votre image ISO,    \n" +
						"    des mises à jour peuvent être disponibles.    \n" +
						"\n    Désirez-vous vérifier les mises à jour disponibles ?    \n" +
						"\n    Votre mot de passe vous sera demandé.    \n")
		vBox.pack_start(label, False, False, 10)
		
		hBox = gtk.HBox()
		bouton1 = gtk.ToggleButton(label = "non merci")
		bouton1.connect("clicked", self.on_button1_clicked)
		bouton2 = gtk.ToggleButton(label = "oui, merci")
		bouton2.connect("clicked", self.on_button2_clicked)
		hBox.add(bouton1)
		hBox.add(bouton2)
		
		vBox.pack_end(hBox,  False, False, 4)
		window.add(vBox)
		window.show_all()
		gtk.main()
		
		
if __name__ == '__main__':
	HandyUpdate()
