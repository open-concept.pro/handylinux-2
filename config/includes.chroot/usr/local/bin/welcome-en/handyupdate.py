#!/usr/bin/python
# -*-coding:utf-8-*

import pygtk
pygtk.require("2.0")
import gtk
import os

class HandyUpdate():
	
	def on_button1_clicked(self, widget):
		gtk.main_quit()
		
	def on_button2_clicked(self, widget):
		os.system("gksudo apt-get update && gpk-update-viewer &")
		gtk.main_quit()
		
	def __init__(self):
		
		window = gtk.Window()
		window.set_title("HandyUpdate")
		window.connect("destroy", self.on_button1_clicked)
		window.set_default_size(300, 200)
		
		vBox = gtk.VBox()
		label = gtk.Label()
		label.set_text("\n    You have just installed HandyLinux ... merci :)    \n" +
						"\n         Your system is ready to use, but              \n" +
						"       depending on your ISO building date,            \n" +
						"          some updates may be available.               \n" +
						"\n    Do you want to check for available update(s) ?    \n" +
						"\n         Your password will be asked.    \n")
		vBox.pack_start(label, False, False, 10)
		
		hBox = gtk.HBox()
		bouton1 = gtk.ToggleButton(label = "no thx")
		bouton1.connect("clicked", self.on_button1_clicked)
		bouton2 = gtk.ToggleButton(label = "yes, thx")
		bouton2.connect("clicked", self.on_button2_clicked)
		hBox.add(bouton1)
		hBox.add(bouton2)
		
		vBox.pack_end(hBox,  False, False, 4)
		window.add(vBox)
		window.show_all()
		gtk.main()
		
		
if __name__ == '__main__':
	HandyUpdate()
