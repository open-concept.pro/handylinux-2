#!/bin/bash
# welcome script on installed system
# fix bookmarks & stuff
##################################

# détection de la langue
TRAD=`echo "${LANG:0:2}"`

if [ "${TRAD}" == "fr" ]; then
    #langue française
    cd /usr/local/bin/welcome
    #session installée
        #mise en place des bookmarks
        echo "file:///home/$USER/Documents" > /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Images"  >> /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Musique"  >> /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Public"  >> /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/T%C3%A9l%C3%A9chargements" >> /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Vid%C3%A9os" >> /home/$USER/.gtk-bookmarks
        #lancement de l'écran d'accueil
        ./welcome.py
        #suppression du lanceur de l'écran d'accueil
        rm /home/$USER/.config/autostart/welcome.desktop
        #suppression des fichiers de conf anglais
        rm /home/$USER/.config/user-dirs.dirs.en
        rm /home/$USER/.config/Thunar/uca.xml.en
else
    #english language
    cd /usr/local/bin/welcome-en
    #installed session
        #traduction des dossiers
        mv /home/$USER/Images /home/$USER/Pictures &
        mv /home/$USER/Musique /home/$USER/Music &
        mv /home/$USER/Téléchargements /home/$USER/Downloads &
        mv /home/$USER/Vidéos /home/$USER/Videos &
        mv /home/$USER/Modèles /home/$USER/Templates &
        mv /home/$USER/Bureau /home/$USER/Desktop &
        #traduction des bookmarks
        echo "file:///home/$USER/Documents" > /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Pictures"  >> /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Music"  >> /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Downloads" >> /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Videos" >> /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Public"  >> /home/$USER/.gtk-bookmarks
        #mise en place des fichiers de conf anglais
        rm /home/$USER/.config/user-dirs.dirs
        mv /home/$USER/.config/user-dirs.dirs.en /home/$USER/.config/user-dirs.dirs
        rm /home/$USER/.config/user-dirs.locale
        rm /home/$USER/.config/Thunar/uca.xml
        mv /home/$USER/.config/Thunar/uca.xml.en /home/$USER/.config/Thunar/uca.xml
        #écran d'accueil
        ./welcome.py
        #suppression de lanceur de l'écran d'accueil
        rm /home/$USER/.config/autostart/welcome.desktop
fi
exit 0
