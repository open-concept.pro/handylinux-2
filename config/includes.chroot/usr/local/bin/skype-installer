#!/bin/bash

# 2015 HandyLinux <contact@handylinux.org>
#
# Simple Skype installer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Simple Skype installer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Simple Skype Installer.  If not, see <http://www.gnu.org/licenses/>.

VERSION="1.3"
DATE="April 27, 2015"
AUTHOR="coyotus"
EMAIL="contact@handylinux.org"
HTTP="http://handylinux.org/"
LICENSE="GPLv3"
RELEASE="2.0"
CODENAME="Jessie"
MODS="arpinux"

if [ `whoami` != root ]; then
		zenity --error --text "<span color=\"red\"><b>Error:</b></span>\nUtilisez gksudo pour lancer l'installeur !"
	exit 1
fi

# détection de la langue
TRAD=$(echo "${LANG:0:2}")

# lancement de l'installer
if [ "${TRAD}" == "fr" ]; then
	wget -O skype-install.deb http://www.skype.com/go/getskype-linux-deb 2>&1 |
	perl -p -e '$| = 1; s/^.* +([0-9]+%) +([0-9,.]+[GMKB]) +([0-9hms,.]+).*$/\1\n# Téléchargement... \2 (\3)/' | \
	zenity --progress --auto-kill --auto-close --text="Téléchargement de Skype. Merci de patienter..."
	if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
		zenity --error --text="Le téléchargement de Skype a échoué. \nCliquez sur Valider pour quitter."
		exit 1
	fi
	apt-get update && apt-get install -y pulseaudio pavucontrol |
	zenity --progress --auto-kill --auto-close --text="Installation des dépendances. Veuillez patienter..."
	if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
		zenity --error --text="L'installation des dépendances a échouée. \nCliquez sur Valider pour quitter."
		exit 1
	fi
	dpkg -i skype-install.deb |
	zenity --progress --auto-kill --auto-close --text="Installation de Skype. Veuillez patienter..."
	if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
		zenity --error --text="Impossible d'installer Skype. \nCliquez sur Valider pour quitter."
		exit 1
	fi
	rm "skype-install.deb" |
	zenity --progress --auto-kill --auto-close --text="Suppression des fichiers d'installation. Merci de patienter..."
	if [ "$?" = 1 ] ; then
		zenity --error --text="L'installation de Skype a échoué. \nCliquez sur Valider pour quitter."
		exit 1
	else
		zenity --info --no-wrap --text="Skype installé avec succès. \nCliquez sur Valider pour quitter et utiliser Skype."
		rm /usr/share/applications/skype-installer.desktop
		exit 0
	fi
else
	wget -O skype-install.deb http://www.skype.com/go/getskype-linux-deb 2>&1 |
	perl -p -e '$| = 1; s/^.* +([0-9]+%) +([0-9,.]+[GMKB]) +([0-9hms,.]+).*$/\1\n# Downloading... \2 (\3)/' | \
	zenity --progress --auto-kill --auto-close --text="Downloading packages. Please wait..."
	if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
		zenity --error --text="Could not install Skype. Click OK to continue."
		exit 1
	fi
	apt-get update && apt-get install pulseaudio pavucontrol |
	zenity --progress --auto-kill --auto-close --text="Installing dependencies. Please wait..."
	if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
		zenity --error --text="Dependencies installation avorted. \nClick OK to continue."
		exit 1
	fi
	dpkg -i skype-install.deb |
	zenity --progress --auto-kill --auto-close --text="Installing package. Please wait..."
	if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
		zenity --error --text="Could not install Skype. Click OK to continue."
		exit 1
	fi
	rm "skype-install.deb" |
	zenity --progress --auto-kill --auto-close --text="Removing existing deb cache files. Please wait..."
	if [ "$?" = 1 ] ; then
		zenity --error --text="Unable to install Skype. Click OK to continue."
		exit 1
	else
		zenity --info --text="Installation successful. Click OK to continue and use Skype."
		rm /usr/share/applications/skype-installer.desktop
		exit 0
	fi
fi
