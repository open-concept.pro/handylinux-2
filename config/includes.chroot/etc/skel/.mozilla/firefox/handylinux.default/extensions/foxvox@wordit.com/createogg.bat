@ECHO OFF
REM  get ogg file path and extension file paths, passed as command line args
set encoderpath=%1
set extpath=%2
set oggfilepath=%3
REM "%extpath%\ConsoleTool.exe" /HIDE

echo "extpath:" %extpath%
echo "encoderpath:" %encoderpath%
echo "oggfilepath:" %oggfilepath%

REM  remove marker indicating to extension that ogg file creation has completed
del %extpath%\oggdone.txt
del %extpath%\noogg.txt

REM  encode the existing wave as ogg file
%encoderpath% -o %oggfilepath% %extpath%\speak.wav

set error=%ERRORLEVEL%
echo "oggenc err: %error%"

    IF %ERRORLEVEL% NEQ 0 (
    echo "Could not find ogg encoder"
    REM create marker file
    echo "ogg not found" > %extpath%\noogg.txt
    ) ELSE (
    REM completed marker file
    echo "OK"
    echo "ogg done" > %extpath%\oggdone.txt
    )

