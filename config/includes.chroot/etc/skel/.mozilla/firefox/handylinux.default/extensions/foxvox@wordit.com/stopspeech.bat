@ECHO OFF
REM extension file paths, passed as command line args
set extpath=%1
REM "%extpath%\ConsoleTool.exe" /HIDE
echo "extpath:" %extpath%

REM remove marker file if taskkill not found
del %extpath%\notaskkill.txt
del %extpath%\taskkilldone.txt

REM stop speech with taskkill command
taskkill.exe /F /IM wv_player*
tskill wv_player*


REM check for these tools from within extension


REM  No longer used. If either tool is missing it will cause an error, so basically always. tskill is on XP home, taskkill on XP Pro and Vista, I think.


REM set error=%ERRORLEVEL%
REM echo "taskkill error: %error%"

REM    IF %ERRORLEVEL% NEQ 0 (
 REM   echo "Could not find taskkill"
    REM create marker file
 REM   echo "taskkill not found" > %extpath%\notaskkill.txt
 REM   ) ELSE (
    REM completed marker file
  
    REM echo "OK"
    echo "taskkill done" > %extpath%\taskkilldone.txt
  
   REM )

