#!/bin/bash

# get mp3 file path and extension file paths, passed as command line args
extpath=$1
mp3path=$2

# remove marker indicating to extension that mp3 file creation has completed
rm -v $extpath/mp3done.txt #>/dev/null
rm -v $extpath/nolame.txt #>/dev/null

# encode the existing wave as mp3 file
lame $extpath/speak.wav $mp3path
error=$?
echo "lame err:$error"

	if [ "$error" -ne "0" ]; then
	{
		#create marker file
		echo "lame not found" > $extpath/nolame.txt
	} fi

	if [ "$error" -eq "0" ]; then
	{
	echo "mp3 done" > $extpath/mp3done.txt
	} fi

